use tokio::io::AsyncRead;

pub struct AsyncReadStreamCounter<R> {
    count: usize,
    inner: R,
}

impl<R> AsyncReadStreamCounter<R> {
    pub fn count(&self) -> usize {
        self.count
    }

    pub fn new(inner: R) -> Self {
        Self { count: 0, inner }
    }
}

impl<R: AsyncRead + Unpin> AsyncRead for AsyncReadStreamCounter<R> {
    fn poll_read(
        mut self: std::pin::Pin<&mut Self>,
        cx: &mut std::task::Context<'_>,
        buf: &mut tokio::io::ReadBuf<'_>,
    ) -> std::task::Poll<std::io::Result<()>> {
        let push = buf.filled().len();
        let res = futures::ready!(std::pin::Pin::new(&mut self.inner).poll_read(cx, buf));
        let pop = buf.filled().len();
        self.count += pop - push;

        std::task::Poll::Ready(res)
    }
}
