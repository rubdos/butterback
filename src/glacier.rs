use std::str::FromStr;

use futures::Stream;
use rusoto_core::Region;
use rusoto_glacier::{
    AbortMultipartUploadInput, DescribeJobInput, GetJobOutputInput, Glacier, InitiateJobInput,
    JobParameters, ListJobsInput,
};
use rusoto_glacier::{CompleteMultipartUploadInput, GlacierClient, UploadMultipartPartInput};

use crate::backup::{ArchiveDescription, CloudTarget, InventoryEntry, JobDescription};

// 128MB chunks
const CHUNK_SIZE: u64 = 128 * 1024 * 1024;
// 1MB chunks
const DOWNLOAD_CHUNK_SIZE: u64 = 1024 * 1024;

pub struct GlacierVault {
    account_id: String,
    vault_name: String,
    region: Region,
}

impl GlacierVault {
    pub fn vault(region: Region, name: String) -> Self {
        GlacierVault {
            account_id: "-".to_string(),
            vault_name: name,
            region,
        }
    }
}

#[derive(serde::Deserialize, Debug)]
#[serde(rename_all = "PascalCase")]
pub struct GlacierVaultInventory {
    #[serde(rename = "VaultARN")]
    pub vault_arn: String,
    pub inventory_date: chrono::DateTime<chrono::Utc>,
    pub archive_list: Vec<GlacierArchiveDescription>,
}

#[derive(serde::Deserialize, serde::Serialize, Debug)]
#[serde(rename_all = "PascalCase")]
pub struct GlacierArchiveDescription {
    pub archive_description: String,
    pub archive_id: String,
    pub creation_date: chrono::DateTime<chrono::Utc>,
    #[serde(rename = "SHA256TreeHash")]
    pub sha256_tree_hash: String,
    pub size: u64,
}

impl From<GlacierArchiveDescription> for ArchiveDescription {
    fn from(f: GlacierArchiveDescription) -> ArchiveDescription {
        ArchiveDescription {
            metadata: serde_json::to_string(&f).unwrap(),
            archive_id: f.archive_id,
            date: f.creation_date,
            description: f.archive_description,
        }
    }
}

impl FromStr for GlacierArchiveDescription {
    type Err = serde_json::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        serde_json::from_str(s)
    }
}

#[derive(Debug, structopt::StructOpt)]
pub enum GlacierRetrievalTier {
    Expedited,
    Standard,
    Bulk,
}

impl std::fmt::Display for GlacierRetrievalTier {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            GlacierRetrievalTier::Expedited => write!(f, "Expedited"),
            GlacierRetrievalTier::Standard => write!(f, "Standard"),
            GlacierRetrievalTier::Bulk => write!(f, "Bulk"),
        }
    }
}

impl Default for GlacierRetrievalTier {
    fn default() -> Self {
        GlacierRetrievalTier::Standard
    }
}

#[derive(Default)]
pub struct GlacierRetrievalParameters {
    pub tier: GlacierRetrievalTier,
}

async fn wait_for_job(
    client: &GlacierClient,
    account_id: &str,
    vault_name: &str,
    job_id: &str,
) -> anyhow::Result<()> {
    let describe = DescribeJobInput {
        account_id: account_id.to_string(),
        job_id: job_id.to_string(),
        vault_name: vault_name.to_string(),
    };
    let res = loop {
        tokio::time::sleep(std::time::Duration::from_secs(60)).await;
        let res = client.describe_job(describe.clone()).await?;
        println!("Progress: {:?}", res);
        if res.completed == Some(true) {
            break res;
        }
    };

    if res.status_code.as_deref() != Some("Succeeded") {
        anyhow::bail!("Could not receive inventory: {:?}", res.status_message);
    }
    Ok(())
}

#[async_trait::async_trait]
impl CloudTarget for GlacierVault {
    type UploadJob = GlacierUploadJob;
    type RetrievalJob = GlacierRetrievalJob;
    type RetrievalParameters = GlacierRetrievalParameters;

    async fn initiate_upload_job(
        &self,
        description: JobDescription,
    ) -> anyhow::Result<Self::UploadJob> {
        let request = rusoto_glacier::InitiateMultipartUploadInput {
            account_id: "-".into(),
            archive_description: Some(description.to_string()),
            part_size: Some(format!("{}", CHUNK_SIZE)),
            vault_name: self.vault_name.clone(),
        };

        let client = GlacierClient::new(self.region.clone());
        let mp_response = client.initiate_multipart_upload(request).await.unwrap();

        Ok(GlacierUploadJob {
            client,
            account_id: self.account_id.clone(),
            upload_id: mp_response
                .upload_id
                .ok_or_else(|| anyhow::format_err!("Malformed response, no upload id returned"))?,
            vault_name: self.vault_name.clone(),
            block: Vec::with_capacity(CHUNK_SIZE as usize),
            complete_checksum: Default::default(),
            total_uploaded: 0,
            creation_date: chrono::Utc::now(),
            description: description.to_string(),
        })
    }

    async fn inventory(&self) -> anyhow::Result<crate::backup::Inventory> {
        let request = rusoto_glacier::DescribeVaultInput {
            account_id: self.account_id.clone(),
            vault_name: self.vault_name.clone(),
        };

        let client = GlacierClient::new(self.region.clone());
        let result = client.describe_vault(request).await?;
        println!("Vault: {:?}", result);

        let job_description = rusoto_glacier::InitiateJobInput {
            account_id: self.account_id.clone(),
            job_parameters: Some(JobParameters {
                archive_id: None,
                description: Some("Butterback `inventory` scan".into()),
                format: Some("JSON".into()),
                inventory_retrieval_parameters: None,
                output_location: None,
                retrieval_byte_range: None,
                sns_topic: None, // XXX that would be nicer
                select_parameters: None,
                tier: None,
                type_: Some("inventory-retrieval".into()),
            }),
            vault_name: self.vault_name.clone(),
        };
        let job = client.initiate_job(job_description).await?;
        println!("Started inventory job: {:?}", job);
        println!("Will poll progress every 60 s");

        wait_for_job(
            &client,
            &self.account_id,
            &self.vault_name,
            job.job_id.as_deref().unwrap(),
        )
        .await?;

        let output = client
            .get_job_output(GetJobOutputInput {
                account_id: self.account_id.clone(),
                job_id: job.job_id.clone().unwrap(),
                range: None,
                vault_name: self.vault_name.clone(),
            })
            .await?;

        let s = std::str::from_utf8(output.body.as_deref().unwrap())?;

        let vault_inventory: GlacierVaultInventory = serde_json::from_str(s)?;
        println!("Vault inventory: {}", vault_inventory.inventory_date);
        let inventory: crate::backup::Inventory = vault_inventory
            .archive_list
            .into_iter()
            .filter_map(|desc| {
                let desc: ArchiveDescription = desc.into();
                let job = match desc.description.parse() {
                    Ok(job) => job,
                    Err(_) => {
                        log::error!(
                            "Could not parse archive {} with description {}",
                            desc.archive_id,
                            desc.description
                        );
                        return None;
                    }
                };
                Some(InventoryEntry { archive: desc, job })
            })
            .collect();
        Ok(inventory)
    }

    async fn continue_job(
        &self,
        archive: &ArchiveDescription,
    ) -> anyhow::Result<Option<Self::RetrievalJob>> {
        let job_metadata: GlacierArchiveDescription = archive.metadata.parse()?;
        let client = GlacierClient::new(self.region.clone());
        let jobs = client
            .list_jobs(ListJobsInput {
                account_id: self.account_id.clone(),
                completed: None,
                limit: None,
                marker: None,
                statuscode: None,
                vault_name: self.vault_name.clone(),
            })
            .await?;

        for job in jobs.job_list.expect("non-empty list") {
            if job.action.unwrap() != "ArchiveRetrieval" {
                log::trace!("Skipping non-archive-retrieval job");
                continue;
            }
            if job.archive_id.unwrap() != job_metadata.archive_id {
                log::trace!("Skipping job with different archive id");
                continue;
            }
            if job.retrieval_byte_range.is_some()
                && job.retrieval_byte_range.as_ref().unwrap()
                    != &format!("0-{}", job.archive_size_in_bytes.unwrap() - 1)
            {
                assert_eq!(
                    job.archive_size_in_bytes.unwrap() as u64,
                    job_metadata.size,
                    "AWS returns different archive size compared to inventory"
                );
                log::warn!("Skipping job with incomplete retrieval");
                continue;
            }

            if job.status_code.as_deref() != Some("Succeeded") {
                wait_for_job(
                    &client,
                    &self.account_id,
                    &self.vault_name,
                    job.job_id.as_deref().unwrap(),
                )
                .await?;
            }

            return Ok(Some(GlacierRetrievalJob {
                client,
                account_id: self.account_id.clone(),
                vault_name: self.vault_name.clone(),

                job_id: job.job_id.unwrap(),

                current_offset: 0,
                expected_length: job_metadata.size,
            }));
        }
        Ok(None)
    }

    async fn initiate_retrieval_job(
        &self,
        archive: &ArchiveDescription,
        params: Self::RetrievalParameters,
    ) -> anyhow::Result<Self::RetrievalJob> {
        let job_metadata: GlacierArchiveDescription = archive.metadata.parse()?;
        let job_param = InitiateJobInput {
            account_id: self.account_id.clone(),
            job_parameters: Some(JobParameters {
                archive_id: Some(job_metadata.archive_id),
                description: None,
                format: None,
                inventory_retrieval_parameters: None,
                output_location: None,
                retrieval_byte_range: None,
                sns_topic: None,
                select_parameters: None,
                tier: Some(params.tier.to_string()),
                type_: Some("archive-retrieval".into()),
            }),
            vault_name: self.vault_name.clone(),
        };

        let client = GlacierClient::new(self.region.clone());
        let job = client.initiate_job(job_param).await?;

        wait_for_job(
            &client,
            &self.account_id,
            &self.vault_name,
            job.job_id.as_deref().unwrap(),
        )
        .await?;

        Ok(GlacierRetrievalJob {
            client,
            account_id: self.account_id.clone(),
            vault_name: self.vault_name.clone(),

            job_id: job.job_id.unwrap(),

            current_offset: 0,
            expected_length: job_metadata.size,
        })
    }
}

#[must_use = "A job is not retrieved unless streamed with `into_blocks()`"]
pub struct GlacierRetrievalJob {
    client: GlacierClient,
    account_id: String,
    vault_name: String,

    job_id: String,

    current_offset: u64,
    expected_length: u64,
    // complete_checksum: StreamingTreeHasher,
    // total_uploaded: usize,
}

impl GlacierRetrievalJob {
    pub fn into_blocks(self) -> impl Stream<Item = std::io::Result<bytes::Bytes>> + Unpin {
        Box::pin(futures::stream::try_unfold(self, |mut state| async {
            match state.next_block().await? {
                Some(body) => Ok(Some((body, state))),
                None => Ok(None),
            }
        }))
    }

    pub async fn next_block(&mut self) -> std::io::Result<Option<bytes::Bytes>> {
        let max_len = std::cmp::min(
            self.expected_length - self.current_offset,
            DOWNLOAD_CHUNK_SIZE,
        );
        if max_len == 0 {
            return Ok(None);
        }
        let output = crate::retry::handle_dispatch_error(|| async {
            self.client
                .get_job_output(GetJobOutputInput {
                    account_id: self.account_id.clone(),
                    job_id: self.job_id.clone(),
                    range: Some(format!(
                        "bytes={}-{}",
                        self.current_offset,
                        self.current_offset + max_len - 1 // last byte *index*, inclusive.
                    )),
                    vault_name: self.vault_name.clone(),
                })
                .await
        })
        .await
        .map_err(|e| std::io::Error::new(std::io::ErrorKind::BrokenPipe, e))?;
        let body = output.body.unwrap();
        assert_eq!(body.len() as u64, max_len);
        if let Some(range) = output.content_range {
            log::trace!("Downloaded range {}", range);
            // bytes 0-500
            let mut range = range.split(|x| x == ' ' || x == '-' || x == '/');
            // XXX We might want to make all of these soft-fails.
            assert_eq!(range.next(), Some("bytes"));
            let offset: u64 = range
                .next()
                .expect("four part range")
                .parse()
                .map_err(|e| std::io::Error::new(std::io::ErrorKind::InvalidData, e))?;
            let end: u64 = range
                .next()
                .expect("four part range")
                .parse()
                .map_err(|e| std::io::Error::new(std::io::ErrorKind::InvalidData, e))?;
            let _total: u64 = range
                .next()
                .expect("four part range")
                .parse()
                .map_err(|e| std::io::Error::new(std::io::ErrorKind::InvalidData, e))?;
            if self.current_offset != offset || self.current_offset + max_len - 1 != end {
                log::warn!(
                    "Range {}-{} is not matching expected {}-{}",
                    offset,
                    end,
                    self.current_offset,
                    self.current_offset + max_len - 1 != end
                );
            }
        }
        if let Some(aws_hash) = output.checksum {
            log::trace!("Validating tree hash");
            let hash = tree_hash(&body);
            assert_eq!(hash, aws_hash);
        }

        self.current_offset += body.len() as u64;
        Ok(Some(body))
    }
}

#[must_use = "Either call finalize() or clean_up()"]
pub struct GlacierUploadJob {
    client: GlacierClient,
    account_id: String,
    upload_id: String,
    vault_name: String,

    creation_date: chrono::DateTime<chrono::Utc>,
    description: String,

    block: Vec<u8>,

    complete_checksum: StreamingTreeHasher,
    total_uploaded: u64,
}

impl GlacierUploadJob {
    async fn upload_block(&mut self) -> anyhow::Result<()> {
        self.complete_checksum.write(&self.block);

        log::debug!("Will write {} bytes", self.block.len());
        let part = UploadMultipartPartInput {
            account_id: self.account_id.clone(),
            // XXX nice and inefficient.
            body: Some(bytes::Bytes::copy_from_slice(&self.block)),
            checksum: Some(tree_hash(&self.block)),
            range: Some(format!(
                "bytes {}-{}/*",
                self.total_uploaded,
                self.total_uploaded + self.block.len() as u64 - 1
            )),
            upload_id: self.upload_id.clone(),
            vault_name: self.vault_name.clone(),
        };
        let result = crate::retry::handle_dispatch_error(|| async {
            self.client.upload_multipart_part(part.clone()).await
        })
        .await?;
        assert_eq!(
            result.checksum.as_deref().map(str::to_lowercase),
            part.checksum.as_deref().map(str::to_lowercase)
        );
        self.total_uploaded += self.block.len() as u64;
        log::trace!(
            "Wrote {} bytes (total {} transmitted)",
            self.block.len(),
            self.total_uploaded
        );
        self.block.clear();

        Ok(())
    }

    pub async fn write(&mut self, mut b: &[u8]) -> anyhow::Result<()> {
        // Here, everything is usize because we're working on a slice.
        while !b.is_empty() {
            let take = std::cmp::min(b.len(), CHUNK_SIZE as usize - self.block.len());
            self.block.extend(&b[..take]);
            b = &b[take..];
            if self.block.len() == CHUNK_SIZE as usize {
                self.upload_block().await?;
            }
        }
        Ok(())
    }

    pub async fn clean_up(self) -> anyhow::Result<()> {
        let command = AbortMultipartUploadInput {
            account_id: "-".into(),
            upload_id: self.upload_id.clone(),
            vault_name: self.vault_name.clone(),
        };
        let result = self.client.abort_multipart_upload(command).await.unwrap();
        println!("{:?}", result);

        Ok(())
    }

    pub async fn finalize(mut self) -> anyhow::Result<ArchiveDescription> {
        if !self.block.is_empty() {
            use anyhow::Context;
            log::debug!("Sending last block of {} bytes", self.block.len());
            if let Err(e) = self.upload_block().await.context("uploading final block") {
                self.clean_up().await.with_context(|| {
                    format!("while cleaning up because of previous error {}", e)
                })?;
                return Err(e);
            }
        }

        log::debug!("Finalizing archive after {} bytes", self.total_uploaded);

        let tree_hash = self.complete_checksum.finalize();
        let command = CompleteMultipartUploadInput {
            checksum: Some(tree_hash.clone()),
            account_id: "-".into(),
            archive_size: Some(format!("{}", self.total_uploaded)),
            upload_id: self.upload_id.clone(),
            vault_name: self.vault_name.clone(),
        };
        let result = self.client.complete_multipart_upload(command).await?;
        log::debug!("{:?}", result);

        Ok(GlacierArchiveDescription {
            archive_id: result
                .archive_id
                .ok_or_else(|| anyhow::format_err!("Upload did not return archive id"))?,
            archive_description: self.description,
            creation_date: self.creation_date,
            sha256_tree_hash: tree_hash,
            size: self.total_uploaded,
        }
        .into())
    }
}

pub fn tree_hash(input_bytes: &[u8]) -> String {
    let mut hasher = StreamingTreeHasher::default();
    hasher.write(input_bytes);
    hasher.finalize()
}

pub struct StreamingTreeHasher {
    next_hash: sha2::Sha256,
    current_content_len: usize,
    leaves: Vec<[u8; 32]>,
}

impl Default for StreamingTreeHasher {
    fn default() -> Self {
        use sha2::Digest;
        StreamingTreeHasher {
            next_hash: sha2::Sha256::new(),
            current_content_len: 0,
            leaves: Vec::new(),
        }
    }
}

impl StreamingTreeHasher {
    pub fn finalize(mut self) -> String {
        use sha2::Digest;
        let mut hasher = self.next_hash;
        if self.current_content_len != 0 || self.leaves.is_empty() {
            self.leaves.push(hasher.finalize_reset().into());
        }

        let mut hashes = self.leaves;

        while hashes.len() != 1 {
            let mut next_hashes = Vec::new();

            for pair in hashes.chunks(2) {
                if pair.len() == 2 {
                    hasher.update(pair[0]);
                    hasher.update(pair[1]);
                    next_hashes.push(hasher.finalize_reset().into());
                } else {
                    next_hashes.push(pair[0]);
                }
            }

            hashes = next_hashes;
        }

        hex::encode(hashes[0])
    }

    pub fn write(&mut self, mut bytes: &[u8]) {
        use sha2::Digest;
        while !bytes.is_empty() {
            const CHUNK: usize = 1024 * 1024;
            let take = std::cmp::min(bytes.len(), CHUNK - self.current_content_len);
            self.next_hash.update(&bytes[..take]);
            bytes = &bytes[take..];
            self.current_content_len += take;

            if self.current_content_len == CHUNK {
                // squeeze and reset
                let hash = self.next_hash.finalize_reset();
                self.current_content_len = 0;
                self.leaves.push(hash.into());
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use rand::RngCore;

    #[test]
    fn hash_uneven_block() {
        let lengths = [
            0,
            1,
            1024 * 1024 - 1,
            1024 * 1024,
            1024 * 1024 + 1,
            1024 * 1024 * 3,
        ];

        for n in &lengths {
            let mut inner = vec![0u8; *n];
            rand::thread_rng().fill_bytes(&mut inner);
            tree_hash(&inner);
            // XXX test vectors could be nice
        }
    }

    #[test]
    fn parse_json() -> anyhow::Result<()> {
        // https://docs.aws.amazon.com/amazonglacier/latest/dev/api-job-output-get.html
        let from_aws = r#"{
 "VaultARN": "arn:aws:glacier:us-west-2:012345678901:vaults/examplevault",
 "InventoryDate": "2011-12-12T14:19:01Z",
 "ArchiveList": [
   {
     "ArchiveId": "DMTmICA2n5Tdqq5BV2z7og-A20xnpAPKt3UXwWxdWsn_D6auTUrW6kwy5Qyj9xd1MCE1mBYvMQ63LWaT8yTMzMaCxB_9VBWrW4Jw4zsvg5kehAPDVKcppUD1X7b24JukOr4mMAq-oA",
     "ArchiveDescription": "my archive1",
     "CreationDate": "2012-05-15T17:19:46.700Z",
     "Size": 2140123,
     "SHA256TreeHash": "6b9d4cf8697bd3af6aa1b590a0b27b337da5b18988dbcc619a3e608a554a1e62"
   },
   {
     "ArchiveId": "2lHzwhKhgF2JHyvCS-ZRuF08IQLuyB4265Hs3AXj9MoAIhz7tbXAvcFeHusgU_hViO1WeCBe0N5lsYYHRyZ7rrmRkNRuYrXUs_sjl2K8ume_7mKO_0i7C-uHE1oHqaW9d37pabXrSA",
     "ArchiveDescription": "my archive2",
     "CreationDate": "2012-05-15T17:21:39.339Z",
     "Size": 2140123,
     "SHA256TreeHash": "7f2fe580edb35154041fa3d4b41dd6d3adaef0c85d2ff6309f1d4b520eeecda3"
   }
  ]
}
        "#;

        let from_nas = r#"{
  "VaultARN": "arn:aws:glacier:eu-north-1:111111111111:vaults/rubdos.be-butterback",
  "InventoryDate": "2021-06-05T20:40:09Z",
  "ArchiveList": [
    {
      "ArchiveId": "Q2TDpjCaUlfQXqaaK00ZejqnjnIuClBIKAalJ0ejOMs4f7jj4yyvxufLIjwBfE8iNBLtfZ2cgwF6WAbhXq-39JEbWGABcj425kgKA34O_RwTQ7oZsP7e3kjdOqnqPlooOUxMz8ymyg",
      "ArchiveDescription": "Butterback backup of /data/foobar-snap/",
      "CreationDate": "2021-06-05T09:26:01Z",
      "Size": 533,
      "SHA256TreeHash": "40ba6be9ca7720503cbba8354bcd2dc1118594f8bd373082ba7909f1549527ab"
    }
  ]
}"#;

        for item in &[from_aws, from_nas] {
            let gvi: GlacierVaultInventory = serde_json::from_str(item)?;
            let _inventory: Vec<ArchiveDescription> =
                gvi.archive_list.into_iter().map(Into::into).collect();
        }

        Ok(())
    }
}
