use std::io::{Read, Write};
use std::path::Path;

use curve25519_dalek::ristretto::{CompressedRistretto, RistrettoBasepointTable, RistrettoPoint};
use curve25519_dalek::scalar::Scalar;

pub struct PublicKey(RistrettoPoint);
pub struct PrivateKey(Scalar);

const G: &RistrettoBasepointTable = &curve25519_dalek::constants::RISTRETTO_BASEPOINT_TABLE;

pub fn generate_keypair() -> (PublicKey, PrivateKey) {
    let private = Scalar::random(&mut rand::thread_rng());
    let public = &private * G;

    (PublicKey(public), PrivateKey(private))
}

impl PublicKey {
    pub fn from_bytes(b: [u8; 32]) -> anyhow::Result<Self> {
        let point = CompressedRistretto::from_slice(&b);

        Ok(Self(point.decompress().ok_or_else(|| {
            anyhow::format_err!("Invalid public key")
        })?))
    }

    pub fn load_from(path: &Path) -> Result<Self, anyhow::Error> {
        anyhow::ensure!(path.is_file(), "{:?} is not a file", path);

        let mut file = std::fs::File::open(path)?;
        let mut bytes = [0u8; 32];
        file.read_exact(&mut bytes)?;

        Self::from_bytes(bytes)
    }

    pub fn write_to(&self, path: &Path) -> Result<(), anyhow::Error> {
        let mut file = std::fs::File::create(path)?;
        let compressed = self.0.compress();
        file.write_all(&compressed.to_bytes())?;

        Ok(())
    }

    pub fn to_bytes(&self) -> [u8; 32] {
        self.0.compress().to_bytes()
    }
}

impl PrivateKey {
    pub fn load_from(path: &Path) -> Result<Self, anyhow::Error> {
        anyhow::ensure!(path.is_file(), "{:?} is not a file", path);

        let mut file = std::fs::File::open(path)?;
        let mut bytes = [0u8; 32];
        file.read_exact(&mut bytes)?;

        Ok(Self(Scalar::from_bits(bytes)))
    }

    pub fn write_to(&self, path: &Path) -> Result<(), anyhow::Error> {
        let mut file = std::fs::File::create(path)?;
        file.write_all(&self.0.to_bytes())?;

        Ok(())
    }

    pub fn agree_with(&self, other: &PublicKey) -> [u8; 32] {
        let shared_secret = self.0 * other.0;
        shared_secret.compress().to_bytes()
    }

    pub fn public(&self) -> PublicKey {
        PublicKey(&self.0 * G)
    }
}
